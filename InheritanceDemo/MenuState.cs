﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace InheritanceDemo
{
    public class MenuState : IGameState
    {

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        ContentManager Content;
        GraphicsDevice graphicsDevice;
        Game1 game;
        private SpriteFont font;

        public MenuState() { }

        public void Initialize(Game1 game, ContentManager c, GraphicsDeviceManager gdm, GraphicsDevice gd)
        {
            // to do
            graphics = gdm;
            graphicsDevice = gd;
            Content = c;
            this.game = game;
        }

        public void Enter()
        {
            // to do
        }
        public void Exit()
        {
            // to do
        }

        public void LoadContent()
        {
            // to do
            spriteBatch = new SpriteBatch(graphicsDevice);
            font = Content.Load<SpriteFont>("Bangers");
        }

        public void UnloadContent()
        {
            // to do
        }

        public void Update(GameTime gameTime)
        {
            // to do
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                game.Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                game.ChangeState(game.playState);
            }

        }

        public void Draw()
        {
            graphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();
            spriteBatch.DrawString(font, "This is the menu!!!", new Vector2(10, 10), Color.White);
            spriteBatch.End();

            // to do
        }
    }
}
