﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace InheritanceDemo
{
    public class PlayState : IGameState
    {

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        ContentManager Content;
        GraphicsDevice graphicsDevice;
        Game1 game;
        private SpriteFont font;


        public PlayState() { }


        public void Initialize(Game1 game, ContentManager c, GraphicsDeviceManager gdm, GraphicsDevice gd)
        {
            graphics = gdm;
            graphicsDevice = gd;
            Content = c;
            this.game = game;
        }

        public void Enter()
        {
            // to do
        }
        public void Exit()
        {
            // to do
        }

        public void LoadContent()
        {
            spriteBatch = new SpriteBatch(graphicsDevice);
            font = Content.Load<SpriteFont>("Bangers");
            // to do - load any extra content...
        }

        public void UnloadContent()
        {
            // to do
        }

        public void Update(GameTime gameTime)
        {
            // to do
            if (Keyboard.GetState().IsKeyDown(Keys.Delete))
            {
                game.ChangeState(game.menuState);
            }
        }

        public void Draw()
        {
            // to do
            graphicsDevice.Clear(Color.AntiqueWhite);

            spriteBatch.Begin();
            spriteBatch.DrawString(font, "This is the game!!!", new Vector2(50, 200), Color.Black);
            spriteBatch.End();
        }
    }
}
